db.getCollection('rooms').insert ({
    name: "single",
        accomodations: 2.0,
       price: 1000.0,
    description: "A simple room with all the basic necessities",
    rooms_available: 10.0,
    isAvailable: false,
    
});
    

db.getCollection('rooms').insertMany ([{
           
    name: "double",
    accomodations: 3.0,
    price: 2000.0,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5.0,
    isAvailable: false,
},
{        name: "queen",
    accomodations: 4.0,
    price: 4000.0,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15.0,
    isAvailable: false,

}])


db.getCollection('rooms').find({name: "double"});


db.getCollection('rooms').updateOne({
    name: "queen" },
    {
        $set: {
            rooms_available: 0
        }
    }
    );
    

db.getCollection('rooms').deleteMany({
        rooms_available: 0
        });
        



