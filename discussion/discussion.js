// Mongodb operations
// for creating 

db.users.insert({
    firstName: "Ely", 
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "Javascript", "Phyton"],
    department: "none"
        
        
    })


// for querying all data in the database
// for finding documents
db.users.find();

db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])


db.users.find();

//Find [one]
db.users.find({firstName: "Francis", age: 61});




//to delete specific, if same data, prioritize last entry
db.users.deleteOne({
    firstName: "Ely"
});

// to add specific
// note data will add last position
db.users.insert(
    {
        firstName: "Gloc-9",
        lastName: "Walang Apelyido",
        age: 43,
        contact: {
            phone: "5336234",
            email: "gloc@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    }

);




    






//UPDATE DATA
//$set to update specific data entry
db.users.updateOne(
    {
        firstName: "Chito"//data you selected or needed
    },
    {
        $set: {
            lastName: "Esguerra"//data to be updated
        }
    }
);


//delete multiple data
db.users.deleteMany({
department: "DOH"//same data groups you want to delete
});


db.users.updateMany(//to update multiple data
    {
        department: "none" //target
    },
    {
        $set: {
            department: "HR" // change to =
        }
    }
);





db.getCollection('rooms').insertMany([
    {
        name: "single",
        accomodations: 2.0,
       price: 1000.0,
    description: "A simple room with all the basic necessities",
    rooms_available: 10.0,
    isAvailable: false,
    {
        name: "double",
        accomodations: 3.0,
       price: 2000.0,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5.0,
    isAvailable: false,
    }
])



